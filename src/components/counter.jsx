import React, {Comment, Component} from 'react';

class Header extends Component {
    render() {
        return (
            <div className='header'>
                <p>Countries</p>
            </div>
        );
    }
}

export default Header;

class Container extends Component {
    state = {};
    render() {
        return (
            <div class="mcontainer">
                <div class="search_box">
                    <input type="text" className="inpBox" placeholder="Search Countries" />
                    <button type="button" className="fetchBtn" onclick="searchData()">
                        <i class="fa fa-search"></i>
                    </button>
                </div>

                <div className="cards_container">
                    <Card />
                </div>
            </div>
        );
    }
}

class Card extends Component {
    state = {
        flagImg: "https://flagcdn.com/w320/in.png",
        nName: "India",
        Crncy: "Rupee",
        Cptl: "New Delhi",
        map: "https://goo.gl/maps/WSk3fLwG4vtPQetp7"
    };
    render() {
        return (
            <div className="mcard">
                <div className="flag">
                    <img className="flag_img" src={this.state.flagImg} alt="flag image" />
                </div>
                <div className="info_area">
                    <div className="info">
                        <p className="name">{this.state.nName}</p>
                        <p className="currency">Currency: {this.state.Crncy}</p>
                        <p className="capital">Capital: {this.state.Cptl}</p>
                    </div>
                    <div className="ref_area">
                        <a href={this.state.map} target="_blank" rel="noopener noreferrer">
                            <span className="map_btn_span">Show Map</span>
                        </a>
                        <a href="" target="_blank" rel="noopener noreferrer">
                            <span className="detail_btn_span">Detail</span>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export {
    Container,
    Card
}
